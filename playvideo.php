<?php
error_reporting(E_ALL);
  ini_set('display_errors', 1);
  include("config/config.php");
?>
<!DOCTYPE html>
<html>
<head>
  <title>Video.js | HTML5 Video Player</title>

  <!-- Chang URLs to wherever Video.js files will be hosted -->
  <link href="video-js/video-js.css" rel="stylesheet" type="text/css">
  <!-- video.js must be in the <head> for older IEs to work. -->
  <script src="video-js/video.js"></script>

  <!-- Unless using the CDN hosted version, update the URL to the Flash SWF -->
  <script>
    videojs.options.flash.swf = "video-js.swf";
  </script>


</head>
<body>

  <?php

    if(isset($_GET['video']))
    {
      $video_id = $_GET['video'];
    }
    else
    {
      $video_id = 1; 
    }  
    $video_res = mysqli_query($db,"SELECT * FROM video WHERE video_id='".$video_id."'");
    $video_details  = $video_res->fetch_assoc();
    $video_file     = "videos/".$video_details['video_file'];
    //die();
  ?>
  <h1 style="text-align:center"><?php echo $video_details['video_title']; ?></hi>

<div style="margin:0 auto; width:650px">
  <video id="example_video_1" class="video-js vjs-default-skin" controls preload="none" width="640" height="264"
      poster="http://video-js.zencoder.com/oceans-clip.png"
      data-setup="{}">
    <source src="<?php echo $video_file; ?>" type='video/mp4' />
    <track kind="captions" src="video-js/demo.captions.vtt" srclang="en" label="English"></track><!-- Tracks need an ending tag thanks to IE9 -->
    <track kind="subtitles" src="video-js/demo.captions.vtt" srclang="en" label="English"></track><!-- Tracks need an ending tag thanks to IE9 -->
    <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
  </video>
</div>
  <a href="videos/flower_rose.mp4"><button>Download</button></a>
</body>
</html>
