<?php
	/*error_reporting(E_ALL);
	ini_set('display_errors', 1);*/
	session_start();
	if(!$_SESSION['adminLoggedIn'])
	{
		header('Location: index.php');
		exit();
	}
	include("../config/config.php");

		$error_message = "";
	if(isset($_POST['submit_video'])  )
	{
		if($_POST['file_name'] != "")
		{

			if(file_exists( "../videos/".$_POST['file_name'] ))
			{

				$insert_query = "INSERT INTO video (video_title, video_file) VALUES ('".$_POST['video_title']."','".$_POST['file_name']."')";
				$inserted = mysqli_query($db,$insert_query);

				$video_id = $db->insert_id;

				$video_play_url = FIXED_IP."/playvideo/playvideo.php?video=".$video_id ;

//				 "video_play_url".$video_play_url;

				$PNG_TEMP_DIR = '../qrcodes/';
    
			    //html PNG location prefix
			    $PNG_WEB_DIR = '../qrcodes/';

			    include "phpqrcode/qrlib.php";    
			    

    			$errorCorrectionLevel = 'H';    
    			$matrixPointSize	=	9;

    			$filename = $PNG_TEMP_DIR.$video_id.'.png';
    			QRcode::png($video_play_url, $filename, $errorCorrectionLevel, $matrixPointSize, 2);    

    			unset($_POST);
    			header('Location: admin.php');

			}
			else
			{
				$error_message = "File not found in the directory!";
			}
		}
		else
		{
			$error_message = "File Name Must not be empty!";
		}
	}
	if(isset($_GET['delete'])  )
	{

				$del_query = "DELETE FROM video WHERE video_id='".$_GET['delete']."'";
				$deleted = mysqli_query($db,$del_query);
				header('Location: admin.php');

	} 

?>
<!doctype html>

<head>

	<!-- Basics -->
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title>Video admin</title>

	<!-- CSS -->
	
	<link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/styles.css">
	
</head>

	<!-- Main HTML -->
	
<body>
	

	<!-- Begin Page Content -->
	
	<div id="containerList">



<form action="admin.php" method="post">
        <fieldset>
            <legend>Add New Video</legend>
           	<p class="errorClass"><?php echo $error_message; ?></p>
            <div>
                <input type="text" name="video_title" placeholder="Video Title"/>
            </div>
            <div>
                <input type="text" name="file_name" placeholder="File Name"/>
            </div>
            <input type="submit" name="submit_video" value="Add Video"/>
        </fieldset>    
</form>

<div style="height:50px; font-size:18px"><h2 style="float:left">Video List</h2>
	<div style="float:right`">
		<form action="regenerateQR.php" method="post">
			<input type="submit" name="submit_video" value="Regenerate QR"/>
		</form>
	</div>
</div>
<table class="bordered">
    <thead>
    <tr>
        <th>#</th>        
        <th>Title</th>
        <th>File Name</th>
        <th>URL</th>
        <th>QR Code</th>
        <th> -Actions- </th>
    </tr>
    </thead>
    <tbody>
		<?php
			$select_video = mysqli_query($db,"SELECT * FROM video");
			$i=0;
			while($row = $select_video->fetch_assoc()) {
    		$i++;	
    		?>
			
			<tr>
		        <td><?php echo $row['video_id'];?></td>        
		        <td><?php echo $row['video_title'] ;?></td>
		        <td><?php echo $row['video_file'] ;?></td>
		        <td>
		        	<?php 
		        	echo $video_play_url = FIXED_IP."/playvideo/playvideo.php?video=".$row['video_id']  ;


		        	?>

		        </td>
		        <td>
		        	<?php

		        	if(file_exists("../qrcodes/".$row['video_id'].".png" ))
		        	{
		        		?>
		        		<a href="../qrcodes/<?php echo $row['video_id']; ?>.png" ><img src="../qrcodes/<?php echo $row['video_id']; ?>.png" width="50px" height="50px"></a>
		        	<?php
		        	}
		        	?>
		        </td>
		        <td> <a href="admin.php?delete=<?php echo $row['video_id']; ?>">Remove</a> </td>
		    </tr>
    		
    		<?php

			}


		?>    
	
    </tbody>
		</table>

		<bd><br>

		</div>
			<!-- End Page Content -->
	</body>

</html>

	
	
	
	
	
		
	