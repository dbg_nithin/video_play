<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	session_start();
	if(!$_SESSION['adminLoggedIn'])
	{
		header('Location: index.php');
		exit();
	}
	include("../config/config.php");
	include "phpqrcode/qrlib.php";    

	$select_video = mysqli_query($db,"SELECT * FROM video");
	while($row = $select_video->fetch_assoc()) {
		$video_id = $row['video_id'];
    	$video_play_url = FIXED_IP."/playvideo/playvideo.php?video=".$video_id ;
		$PNG_TEMP_DIR = '../qrcodes/';
	    //html PNG location prefix
		$PNG_WEB_DIR = '../qrcodes/';
			
    	$errorCorrectionLevel = 'H';    
    	$matrixPointSize	=	9;
    	$filename = $PNG_TEMP_DIR.$video_id.'.png';
    	if(file_exists($filename))
    	{
    		unlink($filename);
    	}

    	QRcode::png($video_play_url, $filename, $errorCorrectionLevel, $matrixPointSize, 2);    
		unset($_POST);

	}
    header('Location: admin.php');
?>