<?php
  /* 
  
  error_reporting(E_ALL);
  ini_set('display_errors', 1); 

  */

  include("config/config.php");

?>
<!DOCTYPE html>
<html>
<head>
  <title>Video List</title>
  <link rel="stylesheet" href="admin/css/styles.css">
  

</head>
<body>
    <div id="containerList">
  <?php
    $select_video = mysqli_query($db,"SELECT * FROM video");
  while($row = $select_video->fetch_assoc()) {
    ?> 
    <div class="videoListContainer">
      <h1 style="text-align:left;"><?php echo $row['video_title']; ?></hi>
      <div style="clear:both"></div>
      <div >
        <div style="float:left">
          <img src="qrcodes/<?php echo $row['video_id']; ?>.png" width="100px" height="100px" >
        </div>
        <div style="float:right;">
          <?php
            $video_play_url = FIXED_IP."/playvideo/playvideo.php?video=".$row['video_id']  ;
          ?>
          <a href="<?php echo $video_play_url; ?>" > 
              <input type="submit" name="submit_video" value="Play Video"/>
          </a>
        </div>
      </div>
    </div>
  <?php
  }
 ?>
  </div>
</body>
</html>
